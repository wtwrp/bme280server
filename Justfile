# Compile aarch64
aarch64:
    cross build --target aarch64-unknown-linux-gnu --release

# Compile armv7
armv7:
    cross build --target armv7-unknown-linux-gnueabihf --release

# Deploy on inventory INV
deploy INV:
    ansible-playbook -i ansible/inv/{{INV}} ansible/deploy.yaml